export const antModalContent = '[class="ant-modal-content"]'
export const buttonAntModalBody = '[class="ant-modal-body"] [type="button"]'
export const createQuestionButton = 'button[data-testid="create-question-btn"]'
export const listItemWrapper = '[data-testid="list-item-wrapper"]'
export const submitButton = 'button[type="submit"]'
export const basicButton = 'button[type="button"]'
export const submitQuestionButton = 'button[data-testid="submit-question-btn"]'

export const antTableCell = 'td[class="ant-table-cell"]'

export const antModalConfirmTitle = '[class="ant-modal-confirm-title"]'

export const antSelectItem = '.ant-select-item'

export const antTabs = '.ant-tabs-tab-btn'

export const exportToWordButton =
  '[data-testid="editor-actions-popup"] [id="exportToWord"]'

export const moreActionsToggle = '[aria-label="More actions"]'

export const alertContainer = '[role="alert"]'

export const ProseMirror = '.ProseMirror'

export const anchorTags = {
  dashboard: 'a[href="/dashboard"]',
  about: 'a[href="/about"]',
  learning: 'a[href="/learning"]',
  discover: 'a[href="/discover"]',
  login: 'a[href="/login"]',
  requestPasswordRest: 'a[href="/request-password-reset"]',
  signup: 'a[href="/signup"]',
  sets: 'a[href="/sets"]',
}

export const navToggle = '[data-testid="nav-toggle"]'
export const notificationPopupContainer = '.ant-notification-notice-message'
