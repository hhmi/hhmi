export const mobile = {
  preset: 'iphone-6',
}

export const laptop = {
  preset: 'macbook-16',
}
