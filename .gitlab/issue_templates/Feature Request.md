<!-- Required. Provide a general summary of the issue in the title above -->

## Context

<!--  Give the necessary context for your proposal. For example, what problem will this feature solve for users? What are the use cases, benefits, and goals? -->

## Proposal

<!-- A precise statement of the proposed feature. -->

## Design

<!-- Include sketches or wireframes of the UI suggested for this feature -->

## Implementation (if applicable)

<!-- A description of the steps to implement the feature.-->

## Alternative approaches (if applicable)

<!-- Include any alternatives to meet this use case. -->

## Scheduling

<!-- Select the relevant option-->

- [ ] This feature is requested for the Phase 2
- [ ] This feature is requested for future enhanced versions



/label ~feature ~scope::backlog
/assign @DioneMentis
