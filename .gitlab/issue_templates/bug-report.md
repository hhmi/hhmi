<!-- Required. Provide a general summary of the issue in the title above -->

## Expected behaviour

<!-- Required. Tell us what should happen. Link to docs if possible. -->

## Current behaviour

<!-- Required. Tell us what happens instead of the expected behaviour. -->

## Steps to reproduce

<!-- Required. Provide a link to a live example or screenshots, and the steps to reproduce this bug.]-->

1.
2.
3.
4.

## Testing environment

<!-- Required. Provide all the relevant below -->

* Username of the user who experienced the problem: 
* URL where the problem occured: 
* Browser name and version: 
* User machine: PC / Mac



## Error logs (if available)

<!-- Provide the server or client logs -->

## Possible solution

<!-- If known, provide details on how to fix the bug.-->

## QA Steps 

[To be completed by Coko]

## Scheduling

<!-- Include any known dependencies and an initial estimation (in hours) for this implementation. -->

[To be completed by Coko]

* Dependencies: [provide issue numbers]
* Development estimate: [provide in hours]

<!-- Bug label is added automatically. After creating this issue you can link other related or blocking issues with the Gitlab's Linked issues functionality. --> 

/label ~bug
/assign @DioneMentis


