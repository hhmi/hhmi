const { logger, useTransaction, uuid, createJWT } = require('@coko/server')
const axios = require('axios').default
const crypto = require('node:crypto')
const qs = require('node:querystring')

const { ChatThread, ChatMessage } = require('@coko/server/src/models')
const { roles } = require('../constants')

const {
  Team,
  User,
  Identity,
  Question,
  QuestionVersion,
  Review,
} = require('../models')

const submitQuestionnaire = async (userId, profileData) => {
  const data = {
    ...profileData,
    profileSubmitted: true,
  }

  return updateUserProfile(userId, data)
}

const updateUserProfile = async (userId, profileData) => {
  try {
    const {
      reviewerInterest: shouldBeReviewer,
      email,
      ...userData
    } = profileData

    return useTransaction(async trx => {
      const isAlreadyReviewer = await User.hasGlobalRole(
        userId,
        roles.REVIEWER,
        {
          trx,
        },
      )

      if (!isAlreadyReviewer && shouldBeReviewer) {
        await Team.addMemberToGlobalTeam(userId, roles.REVIEWER, { trx })
      }

      if (isAlreadyReviewer && !shouldBeReviewer) {
        await Team.removeMemberFromGlobalTeam(userId, roles.REVIEWER, { trx })
      }

      const defaultIdentity = await Identity.findOne(
        {
          userId,
          isDefault: true,
        },
        { trx },
      )

      await defaultIdentity.patch({ email }, { trx })

      const updatedUser = await User.patchAndFetchById(userId, userData, {
        trx,
      })

      return updatedUser
    })
  } catch (e) {
    logger.error(e)
    throw new Error(e)
  }
}

const getDisplayName = async user => User.getDisplayName(user)

const filterUsers = async (params, options = {}) => {
  try {
    const { trx, ...restOptions } = options

    return useTransaction(
      async tr => {
        logger.info(`filter users by query params`)
        return User.filter(params, {
          trx: tr,
          ...restOptions,
        })
      },
      { trx, passedTrxOnly: true },
    )
  } catch (e) {
    logger.error(`error filterUsers: ${e.message}`)
    throw new Error(e)
  }
}

const bioInteractiveLogin = async (authCode, options = {}) => {
  try {
    const {
      BIOINTERACTIVE_OAUTH_CLIENT_ID,
      BIOINTERACTIVE_OAUTH_CLIENT_SECRET,
      BIOINTERACTIVE_OAUTH_REDIRECT_URI,
      BIOINTERACTIVE_OAUTH_TOKEN_URI,
      BIOINTERACTIVE_OAUTH_API_USER_URI,
      BIOINTERACTIVE_OAUTH_AUTH_SERVER_URI,
    } = process.env

    const { trx } = options
    const state = uuid()

    return useTransaction(
      async tr => {
        let tokenResponse

        try {
          const payload = {
            grant_type: 'authorization_code',
            code: authCode,
            callback_url: BIOINTERACTIVE_OAUTH_REDIRECT_URI,
            auth_url: BIOINTERACTIVE_OAUTH_AUTH_SERVER_URI,
            access_token_url: BIOINTERACTIVE_OAUTH_TOKEN_URI,
            client_id: BIOINTERACTIVE_OAUTH_CLIENT_ID,
            client_secret: BIOINTERACTIVE_OAUTH_CLIENT_SECRET,
            redirect_uri: BIOINTERACTIVE_OAUTH_REDIRECT_URI,
            scope: 'openid',
            state,
          }

          tokenResponse = await axios.request({
            url: BIOINTERACTIVE_OAUTH_TOKEN_URI,
            method: 'post',
            data: qs.stringify(payload),
          })
        } catch (e) {
          throw new Error(e)
        }

        const { access_token: accessToken, error: tokenError } =
          tokenResponse.data

        if (tokenError) {
          logger.error(
            `error bioInteractiveLogin: get access token: ${tokenError}`,
          )
          throw new Error(tokenError)
        }

        // get user
        const { data: userInfo, error: userInfoError } = await axios.get(
          BIOINTERACTIVE_OAUTH_API_USER_URI,
          { headers: { Authorization: `Bearer ${accessToken}` } },
        )

        if (userInfoError) {
          logger.error(
            `error bioInteractiveLogin: get access token: ${userInfoError}`,
          )
          throw new Error(userInfoError)
        }

        let user

        // do we already have a user that is social?
        // no, set the user
        const { email } = userInfo

        // const identity = await Identity.query().findOne(builder =>
        //   builder
        //     .whereJsonSupersetOf('profile_data', {
        //       sub, // biointeractive user id
        //     })
        //     .where({
        //       isSocial: true,
        //       provider: 'biointeractive',
        //     }),
        // )

        const identity = await Identity.findOne({
          email: email.toLowerCase(),
          isSocial: true,
          provider: 'biointeractive',
        })

        if (!identity) {
          const givenNames = userInfo.given_name.map(g => g.value).join(' ')
          const surname = userInfo.family_name.map(g => g.value).join(' ')
          const password = uuid()
          const agreedTc = false

          logger.info('bioInteractiveLogin: creating user')

          user = await User.insert(
            {
              agreedTc,
              givenNames: givenNames || 'given_name',
              password,
              surname: surname || 'family_name',
              isActive: true,
            },
            { trx: tr },
          )

          const verificationToken = crypto.randomBytes(64).toString('hex')
          const verificationTokenTimestamp = new Date()

          logger.info(
            'bioInteractiveLogin: creating user local identity with fetched email',
            user,
          )

          await Identity.insert(
            {
              userId: user.id,
              email,
              isSocial: true,
              verificationToken,
              verificationTokenTimestamp,
              isVerified: true,
              isDefault: true,
              oauthAccessToken: accessToken,
              provider: 'biointeractive',
              profileData: userInfo,
            },
            { trx: tr },
          )

          return {
            user,
            token: createJWT(user),
          }
        }

        // get the user, and update the token
        await identity.patch({ oauthAccessToken: accessToken }, { trx: tr })
        user = await User.findById(identity.userId)

        // if (!user.username) {
        //   await User.patchAndFetchById(user.id, {
        //     username: 'incomplete profile',
        //   })
        // }

        return {
          user,
          token: createJWT(user),
        }
      },
      { trx },
    )
  } catch (e) {
    logger.error(`error bioInteractiveLogin: ${e.message}`)
    throw new Error(e)
  }
}

const deleteUsersRelatedItems = async (ids, options = {}) => {
  try {
    return useTransaction(
      async trx => {
        // fetch user items
        const questionsByUser = await Promise.all(
          ids.map(async id => {
            const questions = await Question.findByRole(id, 'author')
            return {
              userId: id,
              questions: questions.result,
            }
          }),
        )

        // for all published items set `deletedAuthorName` field to the author's display name
        await Promise.all(
          questionsByUser.map(async user => {
            const userData = await User.findById(user.userId)
            const authorDisplayName = await User.getDisplayName(userData)

            await Promise.all(
              user.questions.map(async question => {
                // const publishedQuestions = []
                let isPublished = false
                const versions = await Question.getVersions(question.id)
                const versionsIds = versions.result.map(version => version.id)

                versions.result.every(version => {
                  if (version.published) {
                    isPublished = true
                    return false
                  }

                  return true
                })

                if (isPublished) {
                  await Question.updateAndFetchById(question.id, {
                    deletedAuthorName: authorDisplayName,
                  })
                } else {
                  // delete all reviews related to question versions
                  await Review.query(trx)
                    .delete()
                    .whereIn('question_version_id', versionsIds)

                  // delete all versions
                  await QuestionVersion.query(trx).delete().where({
                    questionId: question.id,
                  })

                  // get all chat threads for questions
                  const chatThreads = await ChatThread.find({
                    relatedObjectId: question.id,
                  })

                  const chatThreadsIds = chatThreads.result.map(
                    chatThread => chatThread.id,
                  )

                  // delete all chat messages in those threads
                  await ChatMessage.query(trx)
                    .delete()
                    .whereIn('chatThreadId', chatThreadsIds)

                  // delete the chat threads
                  await ChatThread.query(trx).delete().where({
                    relatedObjectId: question.id,
                  })

                  // delete question
                  await Question.deleteById(question.id)
                }
              }),
            )
          }),
        )

        // additionally, delete all chat messages from these users in other chats
        await ChatMessage.query(trx).delete().whereIn('userId', ids)

        // delete any reviews from these users
        await Review.query(trx).delete().whereIn('reviewerId', ids)

        return true
      },
      { trx: options.trx, passedTrxOnly: true },
    )
  } catch (e) {
    logger.error(`error deleteUsersRelatedItems: ${e.message}`)
    throw new Error(e)
  }
}

const getUserTeams = user => {
  try {
    const { id } = user
    return User.getTeams(id)
  } catch (e) {
    logger.error(`error getUserTeams: ${e.message}`)
    throw new Error(e)
  }
}

module.exports = {
  submitQuestionnaire,
  updateUserProfile,
  filterUsers,
  getDisplayName,
  bioInteractiveLogin,
  deleteUsersRelatedItems,
  getUserTeams,
}
