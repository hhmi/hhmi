module.exports = {
  labels: {
    QUESTION_CONTROLLERS: 'Question controllers',
    LIST_CONTROLLERS: 'list controllers',
    SETS_CONTROLLERS: 'Context-dependent item set controllers',
    NOTIFICATION_CONTROLLERS: 'Notification controllers',
    REPORT_CONTROLLERS: 'Report controllers',
  },
  actions: {
    MESSAGE_CREATED: 'MESSAGE_CREATED',
    NEW_NOTIFICATION: 'NEW_NOTIFICATION',
    DASHBOARD_UPDATED: 'DASHBOARD_UPDATED',
  },
  REVIEWER_STATUSES: {
    accepted: 'acceptedInvitation',
    added: 'notInvited',
    invited: 'invited',
    rejected: 'rejectedInvitation',
    revoked: 'invitationRevoked',
  },
}
