// eslint-disable-next-line import/no-extraneous-dependencies
const { db } = require('@coko/server')

const { logger } = require('@coko/server')

const dbCleaner = async () => {
  const query = await db.raw(
    `SELECT tablename FROM pg_tables WHERE schemaname='public'`,
  )

  const permanentTables = [
    'migrations',
    'resources',
    'course',
    'unit',
    'topic',
    'learning_objective',
    'essential_knowledge',
    'application',
    'skill',
    'understanding',
  ]

  const { rows } = query

  if (rows.length > 0) {
    await Promise.all(
      rows.map(async row => {
        const { tablename } = row

        if (permanentTables.indexOf(tablename) === -1) {
          await db.raw(`TRUNCATE TABLE ${tablename} CASCADE`)
        }

        return true
      }),
    )
    logger.info('[truncateDB]: database cleared')
  }
}

dbCleaner()
