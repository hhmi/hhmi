const roles = {
  AUTHOR: 'author',
  REVIEWER: 'reviewer',
}

module.exports = { roles }
