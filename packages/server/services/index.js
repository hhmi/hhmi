const WaxToDocxConverter = require('./docx/docx.service')
const WaxToScormConverter = require('./scorm/scorm.service')
const WaxToQTIConverter = require('./scorm/scorm.service')

module.exports = { WaxToDocxConverter, WaxToScormConverter, WaxToQTIConverter }
