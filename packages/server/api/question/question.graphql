# TYPES

type Question {
  id: ID!
  versions(latestOnly: Boolean, publishedOnly: Boolean): [QuestionVersion!]!
  agreedTc: Boolean!
  rejected: Boolean!
  author: User
  authorChatThreadId: ID
  productionChatThreadId: ID
  reviewerChatThreadId: ID
  heAssigned: Boolean
  deletedAuthorName: String
  isArchived: Boolean
}

type QuestionVersion {
  id: ID!
  created: DateTime!
  updated: DateTime!

  question: Question!
  content: String

  submitted: Boolean!
  underReview: Boolean!
  inProduction: Boolean!
  published: Boolean!
  unpublished: Boolean!

  publicationDate: DateTime

  topics: [Topic!]!
  courses: [Course!]!

  keywords: [String!]!
  biointeractiveResources: [String!]!

  cognitiveLevel: String
  affectiveLevel: String
  psychomotorLevel: String
  readingLevel: String

  questionType: String
  complexItemSetId: ID
  leadingContent: String
  lastEdit: DateTime

  amountOfReviewers: Int
  isReviewerAutomationOn: Boolean
  reviewerPool: [TeamMember!]!
  reviews(currentUserOnly: Boolean): [Review!]!
  reviewerStatus: String
}

type Topic {
  topic: String!
  subtopic: String
}

type Course {
  course: String!
  units: [Unit!]!
}

type Unit {
  unit: String
  courseTopic: String
  # AP courses
  learningObjective: String
  essentialKnowledge: String
  # IB courses
  application: String
  skill: String
  understanding: String
  # vision and change
  coreConcept: String
  subdiscipline: String
  subdisciplineStatement: String
  coreCompetence: String
  subcompetence: String
  subcompetenceStatement: String
  # aamc
  concept: String
  category: String
}

type QuestionsResponse {
  result: [Question!]!
  totalCount: Int
  relatedQuestionsIds: [ID]
}

type AssignHandlingEditorsResponse {
  questionId: ID!
  hasAuthorshipConflict: Boolean!
  members: [ID!]!
}

# INPUTS

input UpdateQuestionInput {
  agreedTc: Boolean
  content: String

  complexItemSetId: ID
  questionType: String

  topics: [TopicInput!]
  courses: [CourseInput!]

  keywords: [String!]
  biointeractiveResources: [String!]

  cognitiveLevel: String
  affectiveLevel: String
  psychomotorLevel: String
  readingLevel: String
}

input TopicInput {
  topic: String!
  subtopic: String
}

input CourseInput {
  course: String!
  units: [UnitInput!]!
}

input UnitInput {
  unit: String
  courseTopic: String
  # AP courses
  learningObjective: String
  essentialKnowledge: String
  # IB courses
  application: String
  skill: String
  understanding: String
  # vision and change
  coreConcept: String
  subdiscipline: String
  subdisciplineStatement: String
  coreCompetence: String
  subcompetence: String
  subcompetenceStatement: String
  # aamc
  concept: String
  category: String
}

input SidebarFilters {
  topic: String
  subtopic: String
  course: String

  unit: String
  courseTopic: String
  learningObjective: String
  essentialKnowledge: String

  application: String
  skill: String
  understanding: String

  coreConcept: String
  subdiscipline: String
  subdisciplineStatement: String
  coreCompetence: String
  subcompetence: String
  subcompetenceStatement: String

  concept: String
  category: String

  cognitiveLevel: [String]
  questionType: [String]
  complexItemSet: [String]
}

input FilterQuestionsParams {
  filters: SidebarFilters
  searchQuery: String
}

input QuestionPageInput {
  orderBy: String
  ascending: Boolean
  pageSize: Int
  page: Int
}

input DashboardFilters {
  searchQuery: String
  status: String
  heAssigned: String
  author: String
}

enum AdjecentQuestion {
  NEXT
  PREV
}

input GenerateWordFileOptionsInput {
  showFeedback: Boolean
  showMetadata: Boolean
}

# QUERIES & MUTATIONS

extend type Query {
  question(id: ID!): Question!
  getPublishedQuestions(
    params: FilterQuestionsParams
    options: QuestionPageInput
  ): QuestionsResponse!

  getPublishedQuestionsIds: [ID]!

  getAuthorDashboard(
    orderBy: String
    ascending: Boolean
    page: Int
    pageSize: Int
    filters: DashboardFilters
    archived: Boolean
  ): QuestionsResponse!

  getReviewerDashboard(
    orderBy: String
    ascending: Boolean
    page: Int
    pageSize: Int
    filters: DashboardFilters
  ): QuestionsResponse!

  getManagingEditorDashboard(
    orderBy: String
    ascending: Boolean
    page: Int
    pageSize: Int
    filters: DashboardFilters
    archived: Boolean
  ): QuestionsResponse!

  getHandlingEditorDashboard(
    orderBy: String
    ascending: Boolean
    page: Int
    pageSize: Int
    filters: DashboardFilters
    archived: Boolean
  ): QuestionsResponse!

  getInProductionDashboard(
    orderBy: String
    ascending: Boolean
    page: Int
    pageSize: Int
    filters: DashboardFilters
  ): QuestionsResponse!

  getQuestionsHandlingEditors(questionId: ID!): [User!]!
  getAuthorChatParticipants(id: ID!): [User!]!
  getProductionChatParticipants(id: ID!): [User!]!
  getReviewerChatParticipants(questionId: ID!, reviewerId: ID!): [User!]!
}

extend type Mutation {
  createQuestion(input: UpdateQuestionInput): Question!
  duplicateQuestion(questionId: ID!): Question!
  rejectQuestion(questionId: ID!): Question!
  moveQuestionVersionToReview(questionVersionId: ID!): QuestionVersion!
  moveQuestionVersionToProduction(questionVersionId: ID!): QuestionVersion!
  publishQuestionVersion(questionVersionId: ID!): QuestionVersion!
  unpublishQuestionVersion(questionVersionId: ID!): QuestionVersion!
  createNewQuestionVersion(questionId: ID!): Question!

  assignAuthorship(questionId: ID!, userId: ID!): Boolean

  updateQuestion(
    questionId: ID!
    questionVersionId: ID!
    input: UpdateQuestionInput!
  ): Question!

  submitQuestion(
    questionId: ID!
    questionVersionId: ID!
    input: UpdateQuestionInput!
  ): Question!

  generateWordFile(
    questionVersionId: ID!
    options: GenerateWordFileOptionsInput
  ): String!

  generateScormZip(questionVersionId: ID!): String!
  generateQtiZip(questionVersionId: ID!): String!
  uploadFiles(files: [Upload!]!): [File!]!

  assignHandlingEditors(
    questionIds: [ID!]!
    userIds: [ID!]!
  ): [AssignHandlingEditorsResponse!]!
  unassignHandlingEditor(questionId: ID!, userId: ID!): Boolean

  updateReviewerPool(
    questionVersionId: ID!
    reviewerIds: [ID!]!
  ): QuestionVersion!

  changeAmountOfReviewers(
    questionVersionId: ID!
    amount: Int!
  ): QuestionVersion!

  changeReviewerAutomationStatus(
    questionVersionId: ID!
    value: Boolean!
  ): QuestionVersion!

  changeArchiveStatusForItems(
    questionIds: [ID!]!
    isArchiving: Boolean!
    role: String!
  ): Boolean
}

extend type Subscription {
  dashboardUpdate: String!
}
