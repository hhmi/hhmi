const model = require('./archivedItem.model')

module.exports = {
  model,
  modelName: 'ArchivedItem',
}
