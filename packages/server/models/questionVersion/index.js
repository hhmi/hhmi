const model = require('./questionVersion.model')

module.exports = {
  model,
  modelName: 'QuestionVersion',
}
