const model = require('./list.model')

module.exports = {
  model,
  modelName: 'List',
}
