const model = require('./courseMetadata.model')

module.exports = {
  model,
  modelName: 'CourseMetadata',
}
