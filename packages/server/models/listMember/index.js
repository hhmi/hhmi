const model = require('./listMember.model')

module.exports = {
  model,
  modelName: 'ListMember',
}
