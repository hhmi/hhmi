const model = require('./resources.model')

module.exports = {
  model,
  modelName: 'Resource',
}
