const model = require('./report.model')

module.exports = {
  model,
  modelName: 'Report',
}
