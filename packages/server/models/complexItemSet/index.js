const model = require('./complexItemSet.model')

module.exports = {
  model,
  modelName: 'ComplexItemSet',
}
