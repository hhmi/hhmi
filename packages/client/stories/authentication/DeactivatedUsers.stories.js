import React from 'react'
import { DeactivatedUser } from 'ui'

export const Base = () => {
  return <DeactivatedUser />
}

export default {
  title: 'Authentication/Deactivated User',
  component: DeactivatedUser,
}
