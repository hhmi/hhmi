import React from 'react'
import { Footer } from 'ui'

export const Base = () => {
  return <Footer />
}

export default {
  component: Footer,
  title: 'Common/Footer',
}
