import React from 'react'
// import { lorem } from 'faker'

import { Divider } from 'ui'

export const Base = () => <Divider />

export default {
  component: Divider,
  title: 'Common/Divider',
}
