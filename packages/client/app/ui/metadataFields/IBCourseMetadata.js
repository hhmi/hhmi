import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { mapMetadataToSelectOptions } from '../../utilities'
import { Form, Select } from '../common'

const IBCourseMetadata = props => {
  const {
    courseData,
    filterMode,
    getFieldValue,
    isRequired,
    readOnly,
    setFieldsValue,
    unitKey,
    topicKey,
    applicationKey,
    skillKey,
    understandingKey,
    supplementaryKey,
    index,
    showIBoptionalFields,
  } = props

  const metadataMapper = data => mapMetadataToSelectOptions(data, readOnly)

  const unitName = supplementaryKey ? [index, unitKey] : unitKey

  const unitField = supplementaryKey
    ? [supplementaryKey, index, unitKey]
    : unitKey

  const topicName = supplementaryKey ? [index, topicKey] : topicKey

  const topicField = supplementaryKey
    ? [supplementaryKey, index, topicKey]
    : topicKey

  const applicationName = supplementaryKey
    ? [index, applicationKey]
    : applicationKey

  const applicationField = [supplementaryKey, index, applicationKey]

  const skillName = supplementaryKey ? [index, skillKey] : skillKey

  const skillField = [supplementaryKey, index, skillKey]

  const understandingName = supplementaryKey
    ? [index, understandingKey]
    : understandingKey

  const understandingField = [supplementaryKey, index, understandingKey]

  useEffect(() => {
    if (!supplementaryKey) {
      setFieldsValue({
        [unitKey]: null,
        [topicKey]: null,
        [applicationKey]: null,
        [skillKey]: null,
        [understandingKey]: null,
      })
    }
  }, [courseData])

  const handleFrameworkUnitChange = () => {
    if (supplementaryKey) {
      if (getFieldValue(topicField)) {
        const cloned = [...getFieldValue(supplementaryKey)]

        cloned[index] = {
          ...cloned[index],
          [topicKey]: null,
          [applicationKey]: null,
          [skillKey]: null,
          [understandingKey]: null,
        }

        setFieldsValue({
          [supplementaryKey]: cloned,
        })
      }
    } else {
      setFieldsValue({
        [topicKey]: null,
        [applicationKey]: null,
        [skillKey]: null,
        [understandingKey]: null,
      })
    }
  }

  const handleFrameworkTopicChange = () => {
    if (supplementaryKey) {
      if (
        getFieldValue(applicationField) ||
        getFieldValue(skillField) ||
        getFieldValue(understandingField)
      ) {
        const cloned = [...getFieldValue(supplementaryKey)]

        cloned[index] = {
          ...cloned[index],
          [applicationKey]: null,
          [skillKey]: null,
          [understandingKey]: null,
        }

        setFieldsValue({
          [supplementaryKey]: cloned,
        })
      }
    } else {
      setFieldsValue({
        [applicationKey]: null,
        [skillKey]: null,
        [understandingKey]: null,
      })
    }
  }

  const filterCourseUnitOptions = () => {
    return metadataMapper(courseData.units)
  }

  const filterCourseTopicOptions = () => {
    const selectedUnit = getFieldValue(unitField)

    if (selectedUnit) {
      return metadataMapper(
        courseData.topics.filter(a => a.unit === selectedUnit),
      )
    }

    return metadataMapper(courseData.topics)
  }

  const filterApplicationOptions = () => {
    const selectedUnit = getFieldValue(unitField)
    const selectedTopic = getFieldValue(topicField)

    if (selectedTopic) {
      return metadataMapper(
        courseData.applications.filter(a => a.topic === selectedTopic),
      )
    }

    if (selectedUnit) {
      return metadataMapper(
        courseData.applications.filter(a => a.unit === selectedUnit),
      )
    }

    return metadataMapper(courseData.applications)
  }

  const filterUnderstandingOptions = () => {
    const selectedUnit = getFieldValue(unitField)
    const selectedTopic = getFieldValue(topicField)

    if (selectedTopic) {
      return metadataMapper(
        courseData.understandings.filter(a => a.topic === selectedTopic),
      )
    }

    if (selectedUnit) {
      return metadataMapper(
        courseData.understandings.filter(a => a.unit === selectedUnit),
      )
    }

    return metadataMapper(courseData.understandings)
  }

  const filterSkillOptions = () => {
    const selectedUnit = getFieldValue(unitField)
    const selectedTopic = getFieldValue(topicField)

    if (selectedTopic) {
      return metadataMapper(
        courseData.skills.filter(a => a.topic === selectedTopic),
      )
    }

    if (selectedUnit) {
      return metadataMapper(
        courseData.skills.filter(a => a.unit === selectedUnit),
      )
    }

    return metadataMapper(courseData.skills)
  }

  return (
    <>
      {/* {!filterMode && <p>{courseData.label}: College Board Curriculum</p>} */}
      <Form.Item
        label="Course Unit"
        name={unitName}
        rules={[
          isRequired
            ? { required: true, message: 'Course Unit is required' }
            : {},
        ]}
      >
        <Select
          allowClear={filterMode}
          disabled={readOnly}
          onChange={handleFrameworkUnitChange}
          optionFilterProp="label"
          options={filterCourseUnitOptions()}
          showSearch
          wrapOptionText
        />
      </Form.Item>
      <Form.Item dependencies={[unitField]} noStyle>
        {() => (
          <Form.Item
            label="Course Topic"
            name={topicName}
            rules={[
              isRequired
                ? { required: true, message: 'Course Topic is required' }
                : {},
            ]}
          >
            <Select
              allowClear={filterMode}
              disabled={readOnly || (!filterMode && !getFieldValue(unitField))}
              onChange={handleFrameworkTopicChange}
              optionFilterProp="label"
              options={filterCourseTopicOptions()}
              showSearch
              wrapOptionText
            />
          </Form.Item>
        )}
      </Form.Item>
      {showIBoptionalFields && (
        <>
          <Form.Item dependencies={[unitField, topicField]} noStyle>
            {() =>
              !filterApplicationOptions()?.length &&
              (filterMode || !!getFieldValue(topicField)) ? null : (
                <Form.Item
                  label={
                    courseData.value === 'biEnvironmentalScience'
                      ? 'Application and Skill'
                      : 'Application'
                  }
                  name={applicationName}
                >
                  <Select
                    allowClear={filterMode}
                    disabled={
                      readOnly || (!filterMode && !getFieldValue(topicField))
                    }
                    optionFilterProp="label"
                    options={filterApplicationOptions()}
                    showSearch
                    wrapOptionText
                  />
                </Form.Item>
              )
            }
          </Form.Item>
          <Form.Item dependencies={[unitField, topicField]} noStyle>
            {() =>
              courseData.value === 'biEnvironmentalScience' ||
              (!filterSkillOptions()?.length &&
                (filterMode || !!getFieldValue(topicField))) ? null : (
                <Form.Item label="Skill" name={skillName}>
                  <Select
                    allowClear={filterMode}
                    disabled={
                      readOnly || (!filterMode && !getFieldValue(topicField))
                    }
                    optionFilterProp="label"
                    options={filterSkillOptions()}
                    showSearch
                    wrapOptionText
                  />
                </Form.Item>
              )
            }
          </Form.Item>
          <Form.Item dependencies={[unitField, topicField]} noStyle>
            {() => (
              <Form.Item label="Understanding" name={understandingName}>
                <Select
                  allowClear={filterMode}
                  disabled={
                    readOnly || (!filterMode && !getFieldValue(topicField))
                  }
                  optionFilterProp="label"
                  options={filterUnderstandingOptions()}
                  showSearch
                  wrapOptionText
                />
              </Form.Item>
            )}
          </Form.Item>
        </>
      )}
    </>
  )
}

IBCourseMetadata.propTypes = {
  filterMode: PropTypes.bool,
  getFieldValue: PropTypes.func.isRequired,
  isRequired: PropTypes.bool,
  setFieldsValue: PropTypes.func.isRequired,
  readOnly: PropTypes.bool,
  courseData: PropTypes.shape().isRequired,
  unitKey: PropTypes.string,
  topicKey: PropTypes.string,
  applicationKey: PropTypes.string,
  skillKey: PropTypes.string,
  understandingKey: PropTypes.string,
  supplementaryKey: PropTypes.string,
  index: PropTypes.number,
  showIBoptionalFields: PropTypes.bool,
}

IBCourseMetadata.defaultProps = {
  filterMode: false,
  isRequired: false,
  unitKey: 'unit',
  topicKey: 'courseTopic',
  applicationKey: 'application',
  skillKey: 'skill',
  understandingKey: 'understanding',
  readOnly: false,
  supplementaryKey: '',
  index: 0,
  showIBoptionalFields: true,
}

export default IBCourseMetadata
