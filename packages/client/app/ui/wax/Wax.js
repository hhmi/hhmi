import React from 'react'
import PropTypes from 'prop-types'

import { Wax } from 'wax-prosemirror-core'

// import { HhmiLayout } from './layout'
// import { config } from './config'

// const renderImage = file => {
//   const reader = new FileReader()

//   return new Promise((resolve, reject) => {
//     reader.onload = () => resolve(reader.result)
//     reader.onerror = () => reject(reader.error)
//     // Some extra delay to make the asynchronicity visible
//     setTimeout(() => reader.readAsDataURL(file), 150)
//   })
// }

// const t = `<p class="paragraph">Based on the equation below</p><math-display class="math-node">x + y = 5</math-display><p class="paragraph">Which ones are correct?</p><p class="paragraph"></p><div id="" class="mutiple-choice"><div class="mutiple-choice-option" id="d7b65415-ff82-446f-afa4-accaa3837f4a" correct="false" feedback=""><p class="paragraph">answer 1</p><p class="paragraph"><math-inline class="math-node">x+y=1</math-inline></p></div><div class="mutiple-choice-option" id="e7d6bb2f-7cd7-44f1-92a0-281e72157538" correct="true" feedback=""><p class="paragraph">answer 2</p></div><div class="mutiple-choice-option" id="d6fc749f-afae-4203-9562-d68c380a86e5" correct="false" feedback=""><p class="paragraph">answer 3</p></div></div>`

const WaxWrapper = props => {
  const {
    autoFocus,
    content,
    onContentChange,
    onImageUpload,
    config,
    layout,
    readOnly,
    innerRef,
    customValues,
  } = props

  if (!config || !layout) {
    return null
  }

  return (
    <Wax
      autoFocus={autoFocus}
      config={config}
      customValues={customValues}
      // fileUpload={file => renderImage(file)}
      fileUpload={onImageUpload}
      layout={layout}
      onChange={onContentChange}
      readonly={readOnly}
      ref={innerRef}
      targetFormat="JSON"
      value={content}
    />
  )
}

WaxWrapper.propTypes = {
  autoFocus: PropTypes.bool,
  content: PropTypes.shape(),
  onContentChange: PropTypes.func,
  onImageUpload: PropTypes.func,
  config: PropTypes.shape(),
  layout: PropTypes.oneOfType([PropTypes.func, PropTypes.shape()]),
  readOnly: PropTypes.bool,
  innerRef: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.shape() }),
  ]),
  customValues: PropTypes.shape(),
}

WaxWrapper.defaultProps = {
  autoFocus: true,
  content: {},
  onContentChange: () => {},
  onImageUpload: () => {},
  config: {},
  layout: () => {},
  readOnly: false,
  innerRef: () => {},
  customValues: {},
}

export default WaxWrapper
