const sidebarItems = [
  {
    title: 'Course',
    actionType: 'COURSE',
    placeholder: 'Search for courses',
    options: [
      { label: 'course A', value: 'course_A' },
      { label: 'course B', value: 'course_B' },
    ],
  },
  {
    title: 'Question change',
    actionType: 'QUESTION_CHANGE',
    placeholder: 'Search for Question change',
    options: [
      { label: 'question A', value: 'question_A' },
      { label: 'question B', value: 'question_B' },
    ],
  },
  {
    title: 'Vision + change',
    actionType: 'VISION_CHANGE',
    placeholder: 'Search for Vision + change',
    options: [
      { label: 'vision A', value: 'vision_A' },
      { label: 'vision B', value: 'vision_B' },
    ],
  },
  {
    title: 'Unit',
    actionType: 'UNIT',
    placeholder: 'Search for unit',
    options: [
      { label: 'unit A', value: 'unit_A' },
      { label: 'unit B', value: 'unit_B' },
    ],
  },
  {
    title: 'Blooms Level',
    actionType: 'BLOOMS_LEVEL',
    placeholder: 'Search by blooms level',
    options: [
      { label: 'level A', value: 'level_A' },
      { label: 'level B', value: 'level_B' },
    ],
  },
  {
    title: 'Reading Level',
    actionType: 'READING_LEVEL',
    placeholder: 'Search by reading level',
    options: [
      { label: 'level A', value: 'level_A' },
      { label: 'level B', value: 'level_B' },
    ],
  },
  {
    title: 'Future Physicians',
    actionType: 'FUTURE_PHYSICIANS',
    placeholder: 'Search for future phisicians',
    options: [
      { label: 'filter A', value: 'filter_A' },
      { label: 'filter B', value: 'filter_B' },
    ],
  },
  {
    title: 'Learning Objectives',
    actionType: 'LEARNING_OBJECTIVES',
    placeholder: 'Search for learning objectives',
    options: [
      { label: 'learning objective A', value: 'learning_objective_A' },
      { label: 'learning objective B', value: 'learning_objective_B' },
    ],
  },
]

export default sidebarItems
