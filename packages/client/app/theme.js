import { css } from 'styled-components'
import { lighten } from '@coko/client'

const theme = {
  colorBackground: '#ffffff',
  colorBackgroundHue: '#f5f5f5',
  colorBody: '#000000',
  colorPrimary: '#178387',
  colorSecondary: '#0a5c5f', // 'gainsboro',
  colorTertiary: '#8ac341',
  colorBorder: '#777777',
  colorBorderAlt: '#319494',
  colorSelection: '#3fc2cd',
  colorPrimaryBorder: '#71ada9',

  colorInfo: '#178387',
  colorChat: '#178369',
  colorSuccess: '#00763a',
  colorSuccessAlt: '#48a535',
  colorError: '#d43131',
  colorErrorAlt: '#cc4747',
  colorWarning: '#a65b00',

  colorText: '#3F3F3F',
  colorTextDark: '#222222',
  colorTextReverse: '#ffffff',
  colorTextPlaceholder: '#777777',

  fontInterface: 'Arial, sans-serif',
  // font sizes
  fontSize: '16', // actual antd font variable

  fontSizeBase: '16px',
  fontSizeBaseSmall: '14px',
  fontSizeBaseSmaller: '12px',
  fontSizeBaseSmallest: '11px',
  fontSizeHeading1: '54px',
  fontSizeHeading2: '36px',
  fontSizeHeading3: '36px',
  fontSizeHeading4: '36px',
  // fontSizeHeading5: '25px',
  // fontSizeHeading6: '16px',

  // outline width
  lineWidth: '0.5',
  lineWidthBold: '0.5',

  // line heights
  lineHeight: '1.375',
  lineHeightBase: '30px',
  // lineHeightBaseSmall: '32px',
  // lineHeightHeading1: '96px',
  // lineHeightHeading2: '80px',
  // lineHeightHeading3: '59px',
  // lineHeightHeading4: '43px',
  // lineHeightHeading5: '28px',
  // lineHeightHeading6: '31px',

  // fontSizeBase: '1rem', // 16px
  // fontSizeBaseSmall: '0.875rem', // 14px

  gridUnit: '4px',

  borderRadius: '0px',
  borderWidth: '1px',
  borderStyle: 'solid',

  boxShadow: `rgb(0 0 0 / 8%) 0 6px 16px 0, rgb(0 0 0 / 12%) 0 3px 6px -4px, rgba(0 0 0 / 5%) 0 9px 28px 8px`,

  // #region header variables
  mobileLogoHeight: '44px',
  headerPaddingVertical: '10px',
  headerPaddingHorizontal: '24px',
  // #endregion header variables

  mediaQueries: {
    small: '600px',
    medium: '900px',
    mediumPlus: '1024px',
    large: '1200px',
  },

  cssOverrides: {
    ui: {
      Ribbon: css`
        background: ${props => {
          const { status } = props
          if (status === 'success') return props.theme.colorSuccess
          if (status === 'error' || status === 'danger')
            return props.theme.colorError
          return lighten(props.theme.colorBorder, 0.5)
        }};
        color: ${props => {
          const { status } = props
          if (status === 'success' || status === 'error' || status === 'danger')
            return props.theme.colorTextReverse
          return props.theme.colorTextDark
        }};
      `,
    },
  },
}

export default theme
