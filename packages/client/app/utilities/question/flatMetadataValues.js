const metadata = {
  topics: [
    {
      label: 'Biochemistry & Molecular Biology',
      value: 'biochemistryMolecularBiology',
    },
    {
      label: 'Genetics',
      value: 'genetics',
    },
    {
      label: 'Cell biology',
      value: 'cellBiology',
    },
    {
      label: 'Diversity of life',
      value: 'diversityOfLife',
    },
    {
      label: 'Anatomy & Physiology',
      value: 'anatomyPhysiology',
    },
    {
      label: 'Evolution',
      value: 'evolution',
    },
    {
      label: 'Ecology',
      value: 'ecology',
    },
    {
      label: 'Environmental science',
      value: 'environmentalScience',
    },
    {
      label: 'Earth science',
      value: 'earthScience',
    },
    {
      label: 'Science practices',
      value: 'sciencePractices',
    },
  ],
  subtopics: [
    {
      label: 'General Chemistry',
      value: 'generalChemistry',
    },
    {
      label: 'Water and Carbon',
      value: 'waterAndCarbon',
    },
    {
      label: 'Macromolecules',
      value: 'macromolecules',
    },
    {
      label: 'Enzymes & Reactions',
      value: 'enzymesReactions',
    },
    {
      label: 'DNA & RNA',
      value: 'dnaRna',
    },
    {
      label: 'Biotechnology',
      value: 'biotechnology',
    },
    {
      label: 'Gene Expression & Regulation',
      value: 'geneExpressionRegulation',
    },
    {
      label: 'Patterns of Inheritance',
      value: 'patternsOfInheritance',
    },
    {
      label: 'Genomics',
      value: 'genomics',
    },
    {
      label: 'Genetic Disease',
      value: 'geneticDisease',
    },
    {
      label: 'Mutations',
      value: 'mutations',
    },
    {
      label: 'Epigenetics',
      value: 'epigenetics',
    },
    {
      label: 'Bioinformatics',
      value: 'bioinformatics',
    },
    {
      label: 'Cellular Transport',
      value: 'cellularTransport',
    },
    {
      label: 'Cell Structure & Function',
      value: 'cellStructureFunction',
    },
    {
      label: 'Cell Cycle',
      value: 'cellCycle',
    },
    {
      label: 'Cell Communication',
      value: 'cellCommunication',
    },
    {
      label: 'Cellular Energetics',
      value: 'cellularEnergetics',
    },
    {
      label: 'Differentiation',
      value: 'differentiation',
    },
    {
      label: 'Stem Cells',
      value: 'stemCells',
    },
    {
      label: 'Meiosis',
      value: 'meiosis',
    },
    {
      label: 'Bacteria',
      value: 'bacteria',
    },
    {
      label: 'Viruses',
      value: 'viruses',
    },
    {
      label: 'Archaea',
      value: 'archaea',
    },
    {
      label: 'Eukaryotes',
      value: 'eukaryotes',
    },
    {
      label: 'Nervous & Endocrine Systems',
      value: 'nervousEndocrineSystems',
    },
    {
      label: 'Immune System',
      value: 'immuneSystem',
    },
    {
      label: 'Cardiovascular System',
      value: 'cardiovascularSystem',
    },
    {
      label: 'Skin & Musculoskeletal System',
      value: 'skinMusculoskeletalSystem',
    },
    {
      label: 'Metabolism & Nutrition',
      value: 'metabolismNutrition',
    },
    {
      label: 'Reproduction & Development',
      value: 'reproductionDevelopment',
    },
    {
      label: 'Homeostasis',
      value: 'homeostasis',
    },
    {
      label: 'Animal Behavior',
      value: 'animalBehavior',
    },
    {
      label: 'Zoology',
      value: 'zoology',
    },
    {
      label: 'Plant Anatomy & Physiology',
      value: 'plantAnatomyPhysiology',
    },
    {
      label: 'Artificial Selection',
      value: 'artificialSelection',
    },
    {
      label: 'Natural Selection',
      value: 'naturalSelection',
    },
    {
      label: 'Speciation',
      value: 'speciation',
    },
    {
      label: 'Population Genetics',
      value: 'populationGenetics',
    },
    {
      label: 'Phylogeny',
      value: 'phylogeny',
    },
    {
      label: 'Paleobiology',
      value: 'paleobiology',
    },
    {
      label: 'Human Evolution',
      value: 'humanEvolution',
    },
    {
      label: 'Extinction',
      value: 'extinction',
    },
    {
      label: 'Populations',
      value: 'populations',
    },
    {
      label: 'Communities',
      value: 'communities',
    },
    {
      label: 'Ecosystems',
      value: 'ecosystems',
    },
    {
      label: 'Biomes',
      value: 'biomes',
    },
    {
      label: 'Matter & Energy',
      value: 'matterEnergy',
    },
    {
      label: 'Human Population & Impacts',
      value: 'humanPopulationImpacts',
    },
    {
      label: 'Conservation',
      value: 'conservation',
    },
    {
      label: 'Climate Change',
      value: 'climateChange',
    },
    {
      label: 'Natural Resources',
      value: 'naturalResources',
    },
    {
      label: 'Environmental Systems',
      value: 'environmentalSystems',
    },
    {
      label: 'Earth History',
      value: 'earthHistory',
    },
    {
      label: 'Geology',
      value: 'geology',
    },
    {
      label: 'Atmosphere',
      value: 'atmosphere',
    },
    {
      label: 'Climate',
      value: 'climate',
    },
    {
      label: 'Hydrosphere',
      value: 'hydrosphere',
    },
    {
      label: 'Origin of Life',
      value: 'originOfLife',
    },
    {
      label: 'Biogeochemical Cycles',
      value: 'biogeochemicalCycles',
    },
    {
      label: 'Experimental Design',
      value: 'experimentalDesign',
    },
    {
      label: 'Explanations & Argumentation',
      value: 'explanationsArgumentation',
    },
    {
      label: 'Models & Simulations',
      value: 'modelsSimulations',
    },
    {
      label: 'Careers in Science',
      value: 'careersInScience',
    },
    {
      label: 'Science & Society',
      value: 'scienceSociety',
    },
    {
      label: 'Data Analysis',
      value: 'dataAnalysis',
    },
    {
      label: 'Graph Interpretation',
      value: 'graphInterpretation',
    },
    {
      label: 'Statistics',
      value: 'statistics',
    },
  ],
  blooms: {
    cognitive: [
      {
        label: 'Understand (higher-order)',
        value: 'higher-understand',
      },
      {
        label: 'Apply',
        value: 'higher-apply',
      },
      {
        label: 'Analyze',
        value: 'higher-analyze',
      },
      {
        label: 'Evaluate',
        value: 'higher-evaluate',
      },
      {
        label: 'Create',
        value: 'higher-create',
      },
      {
        label: 'Remember',
        value: 'lower-remember',
      },
      {
        label: 'Understand (lower-order)',
        value: 'lower-understand',
      },
    ],
    affective: [
      {
        label: 'Receiving',
        value: 'receiving',
      },
      {
        label: 'Responding',
        value: 'responding',
      },
      {
        label: 'Valuing',
        value: 'valuing',
      },
      {
        label: 'Organization',
        value: 'organization',
      },
      {
        label: 'Characterization',
        value: 'characterization',
      },
    ],
    psychomotor: [
      {
        label: 'Reflex',
        value: 'reflex',
      },
      {
        label: 'Basic fundamental movements',
        value: 'basicFundamentalMovements',
      },
      {
        label: 'Perceptual abilities',
        value: 'perceptualAbilities',
      },
      {
        label: 'Physical abilities',
        value: 'physicalAbilities',
      },
      {
        label: 'Skilled movements',
        value: 'skilledMovements',
      },
      {
        label: 'Non-discursive communication',
        value: 'nonDiscursiveCommunication',
      },
    ],
  },
}

module.exports = metadata
