export default [
  {
    label: 'Context-dependent item set 1',
    value: 'cis1',
  },
  {
    label: 'The Genetics of Tusklessness in Elephants',
    value: 'cis2',
  },
  {
    label: 'Monarch butterfly population decline',
    value: 'cis3',
  },
  {
    label: 'Climate change effects on health',
    value: 'cis4',
  },
  {
    label: 'Weighing the Evidence for a Mass Extinction: In the Ocean',
    value: 'cis5',
  },
  {
    label: 'Biology of SARS-CoV-2',
    value: 'cis6',
  },
]
