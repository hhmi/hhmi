import React, { useState } from 'react'
import { useCurrentUser } from '@coko/client'
import { UserList, Result } from 'ui'
import { Link } from 'react-router-dom'
import { useQuery, useMutation } from '@apollo/client'
import {
  FILTER_USERS,
  DELETE_USERS,
  DEACTIVATE_USERS,
  ACTIVATE_USERS,
  DELETE_RELATED_ITEMS,
} from '../graphql'
import { hasGlobalRole } from '../utilities'

const usersApiToUi = users => {
  if (!users) return []

  return users.map(user => {
    let isReviewer = false

    if (user.teams && user.teams.length) {
      user.teams.forEach(team => {
        if (team.role === 'reviewer') isReviewer = true
      })
    }

    return {
      id: user.id,
      displayName: user.displayName,
      email: user.defaultIdentity.email,
      expertise: user.coursesTeaching,
      isReviewer,
      signUpDate: user.created,
    }
  })
}

const PAGE_SIZE = 10

const ManageUsers = () => {
  const [currentPage, setCurrentPage] = useState(0)
  const [selectedRows, setSelectedRows] = useState([])
  const [search, setSearch] = useState('')
  const [showDeactivated, setShowDeactivated] = useState(false)

  const { currentUser } = useCurrentUser()

  const { loading: usersLoading, data: usersData } = useQuery(FILTER_USERS, {
    variables: {
      params: {
        isActive: !showDeactivated,
        search,
      },
      options: {
        page: currentPage,
        pageSize: PAGE_SIZE,
      },
    },
  })

  const [deleteUsersMutation] = useMutation(DELETE_USERS, {
    update(cache) {
      cache.modify({
        fields: {
          filterUsers() {},
        },
      })
    },
    onCompleted({ deleteUsers }) {
      const total = usersData?.filterUsers.totalCount
      const nrOfPages = Math.ceil(total / PAGE_SIZE)
      const usersInCurrentPage = usersData?.filterUsers.result.length

      // if current page is the last page && you delete all users in that page, load currentPage - 1
      if (
        currentPage === nrOfPages - 1 &&
        usersInCurrentPage === deleteUsers.length
      ) {
        setCurrentPage(currentPage - 1)
      }
    },
  })

  const [deleteUsersRelatedItems] = useMutation(DELETE_RELATED_ITEMS)

  const [deactivateUsersMutation] = useMutation(DEACTIVATE_USERS, {
    update(cache) {
      cache.modify({
        fields: {
          filterUsers() {},
        },
      })
    },
    onCompleted({ deactivateUsers }) {
      const total = usersData?.filterUsers.totalCount
      const nrOfPages = Math.ceil(total / PAGE_SIZE)
      const usersInCurrentPage = usersData?.filterUsers.result.length

      // if current page is the last page && you deactivate all users in that page, load currentPage - 1
      if (
        currentPage === nrOfPages - 1 &&
        usersInCurrentPage === deactivateUsers.length
      ) {
        setCurrentPage(currentPage - 1)
      }
    },
  })

  const [activateUsersMutation] = useMutation(ACTIVATE_USERS, {
    update(cache) {
      cache.modify({
        fields: {
          filterUsers() {},
        },
      })
    },
    onCompleted({ activateUsers }) {
      const total = usersData?.filterUsers.totalCount
      const nrOfPages = Math.ceil(total / PAGE_SIZE)
      const usersInCurrentPage = usersData?.filterUsers.result.length

      // if current page is the last page && you activate all users in that page, load currentPage - 1
      if (
        currentPage > 1 &&
        currentPage === nrOfPages - 1 &&
        usersInCurrentPage === activateUsers.length
      ) {
        setCurrentPage(currentPage - 1)
      }
    },
  })

  const handleDeleteUsers = async vars => {
    await deleteUsersRelatedItems(vars)
    await deleteUsersMutation(vars)
  }

  const handlePageChange = page => {
    setCurrentPage(page - 1)
  }

  const handleSearch = query => {
    setCurrentPage(0)
    setSearch(query)
  }

  const handleShowDeactivatedChange = () => {
    setSelectedRows([])
    setShowDeactivated(!showDeactivated)
  }

  if (!hasGlobalRole(currentUser, 'admin')) {
    return (
      <Result
        // replace link with a Button with to="/dashboard" after MR is merged
        extra={<Link to="/dashboard">Back to Dashboard</Link>}
        status="403"
        subTitle="Sorry, you are not authorized to access this page."
        title="403"
      />
    )
  }

  return (
    <UserList
      currentPage={currentPage + 1}
      currentUserId={currentUser.id}
      data={usersApiToUi(usersData?.filterUsers.result)}
      loading={usersLoading}
      onBulkActivate={activateUsersMutation}
      onBulkDeactivate={deactivateUsersMutation}
      onBulkDelete={handleDeleteUsers}
      onClickShowDeactivated={handleShowDeactivatedChange}
      onPageChange={handlePageChange}
      onSearch={handleSearch}
      pageSize={PAGE_SIZE}
      selectedRows={selectedRows}
      setSelectedRows={setSelectedRows}
      showDeactivated={showDeactivated}
      totalUserCount={usersData?.filterUsers.totalCount}
    />
  )
}

ManageUsers.propTypes = {}

ManageUsers.defaultProps = {}

export default ManageUsers
